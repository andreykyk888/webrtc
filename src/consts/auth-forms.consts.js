const SIGN_UP_FIELDS = {
    firstName: 'First name',
    lastName: 'Last name',
    username: 'Login'
};

const SIGN_IN_FIELDS = {
    username: 'Login'
};

const SIGN_IN_BTN_TITLE = 'Sign In';
const SIGN_UP_BTN_TITLE = 'Sign Up';
const CANCEL_BTN_TITLE = 'Cancel';

export {
    SIGN_UP_FIELDS,
    SIGN_IN_FIELDS,
    SIGN_IN_BTN_TITLE,
    SIGN_UP_BTN_TITLE,
    CANCEL_BTN_TITLE
}
