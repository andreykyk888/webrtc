const SEND_CALL_FIELDS = {
    username: 'Login'
};

const SEND_CALL_BTN_TITLE = 'Call'

const CANCEL_BTN_TITLE = 'Cancel';

export  {
    SEND_CALL_FIELDS,
    SEND_CALL_BTN_TITLE,
    CANCEL_BTN_TITLE
}