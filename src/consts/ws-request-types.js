const RTC_TYPES = {
    offer: 'offer',
    answer: 'answer',
}

const WS_REQUEST_TYPES = {
    initData: 'init',
    sendOffer: RTC_TYPES.offer,
    sendAnswer: RTC_TYPES.answer,
    saveOffer: 'saved-offer',
    saveAnswer: 'saved-answer',
    cancelOffer: 'cancel-offer',
    cancelAnswer: 'cancel-answer',
    completeCall: 'complete-call'
}

export { WS_REQUEST_TYPES, RTC_TYPES };
