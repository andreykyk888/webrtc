const CONNECTED_NO_RECONNECT = {
    isConnected: true,
    isReconnect: false
};

const ISNT_CONNECTED_NO_RECONNECT = {
    isConnected: false,
    isReconnect: false
};

const ISNT_CONNECTED_RECONNECT = {
    isConnected: false,
    isReconnect: true
};

export {CONNECTED_NO_RECONNECT, ISNT_CONNECTED_NO_RECONNECT, ISNT_CONNECTED_RECONNECT}
