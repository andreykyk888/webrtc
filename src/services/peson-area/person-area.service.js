import StoreWorkerService from "../shared/store/store-worker.service";
import PersonAreaWsService from "./ws/person-area-ws.service";
import { WS_REQUEST_TYPES, RTC_TYPES } from "../../consts/ws-request-types";
import PersonAreaWebrtc from "./person-area-webrtc";
import { take } from "rxjs";

export default class PersonAreaService {
    static personAreaWebRTC;
    static offerData;

    static selectAction(jsonData) {
        const data = JSON.parse(jsonData);
        switch (data.reqType) {
            case RTC_TYPES.offer:
                PersonAreaService.offerData = data;
                StoreWorkerService.updateCallState('incomingCall$', data);
                break;
            case RTC_TYPES.answer:
                PersonAreaService.saveAnswer(data);
                break;
            case WS_REQUEST_TYPES.cancelOffer:
                PersonAreaService.cancelFromSender();
                break;
            case WS_REQUEST_TYPES.cancelAnswer:
                PersonAreaService.cancelFromReceiver();
                break;
            default:
                return;
        }
    }

    static sendUserData() {
        const user = StoreWorkerService.userStateValue;
        const data = {
            ...user,
            reqType: WS_REQUEST_TYPES.initData
        };
        const jsonData = JSON.stringify(data);
        PersonAreaWsService.sendData(jsonData);
    }

    static listenMessages() {
        return PersonAreaWsService.listenMessages();
    }


    static sendOfferData(receiverData) {
        PersonAreaService.personAreaWebRTC = new PersonAreaWebrtc();
        PersonAreaService.personAreaWebRTC.createRTCOffer()
            .pipe(take(1))
            .subscribe((offer) => {
                const sendingData = PersonAreaService.createOfferData(receiverData, offer);
                console.log(offer.candidate);
                const jsonSendingData = JSON.stringify(sendingData);
                StoreWorkerService.updateCallState('outgoingCall$', sendingData);
                PersonAreaWsService.sendData(jsonSendingData);
            });
    }

    static createOfferData(receiverData, offer) {
        const user = StoreWorkerService.userStateValue;
        return {
            sender: user,
            receiverUsername: receiverData,
            reqType: RTC_TYPES.offer,
            rtcData: offer,
        };
    }

    static sendAnswerData() {
        PersonAreaService.personAreaWebRTC = new PersonAreaWebrtc();
        PersonAreaService.personAreaWebRTC.createRTCAnswer(PersonAreaService.offerData.rtcData)
            .pipe(take(1))
            .subscribe(answer => {
                const sendingData = PersonAreaService.createAnswerData(PersonAreaService.offerData, answer);
                console.log(answer.candidate);
                PersonAreaWsService.sendData(sendingData);
            });
    }

    static createAnswerData(offerData, answer) {
        const user = StoreWorkerService.userStateValue;
        const data = {
            sender: user,
            receiverUsername: offerData.sender.username,
            reqType: RTC_TYPES.answer,
            rtcData: answer
        };
        return JSON.stringify(data);
    }

    static saveAnswer(answerData) {
        PersonAreaService.personAreaWebRTC.saveAnswer(answerData.rtcData);
    }

    static cancelFromReceiver() {
        PersonAreaService.personAreaWebRTC.cancelConnection();
        StoreWorkerService.updateCallState('outgoingCall$', undefined);
    }

    static cancelFromSender() {
        if(PersonAreaService.personAreaWebRTC) PersonAreaService.personAreaWebRTC.cancelConnection();
        StoreWorkerService.updateCallState('incomingCall$', undefined);
    }

    static cancelOffer() {
        PersonAreaService.personAreaWebRTC.cancelConnection();
        const data = PersonAreaService.createCancelOfferData();
        StoreWorkerService.updateCallState('outgoingCall$', undefined);
        PersonAreaWsService.sendData(data);
    }

    static cancelAnswer() {
        const data = PersonAreaService.createCancelAnswerData();
        StoreWorkerService.updateCallState('incomingCall$', undefined);
        PersonAreaWsService.sendData(data);
    }

    static createCancelAnswerData() {
        const user = StoreWorkerService.userStateValue;
        const sender = StoreWorkerService.getCallState('incomingCall$').sender;
        const data = {
            sender: user,
            receiverUsername: sender.username,
            reqType: WS_REQUEST_TYPES.cancelAnswer,
            rtcData: undefined
        };
        return JSON.stringify(data);
    }

    static createCancelOfferData() {
        const user = StoreWorkerService.userStateValue;
        const sendingData = StoreWorkerService.getCallState('outgoingCall$');
        const data = {
            sender: user,
            receiverUsername: sendingData.receiverUsername,
            reqType: WS_REQUEST_TYPES.cancelOffer,
            rtcData: undefined
        };
        return JSON.stringify(data);
    }

    static cancelConnection() {
        PersonAreaService.personAreaWebRTC.cancelConnection();
    }

}
