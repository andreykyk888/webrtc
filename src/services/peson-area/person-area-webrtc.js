import { from, Subject, take } from "rxjs";
import {
    RTCPeerConnection,
    RTCIceCandidate,
    RTCSessionDescription,
    mediaDevices,
    MediaStream,
} from 'react-native-webrtc'
import { ICE_SERVER_CONFIG } from "../../configs";
import StoreWorkerService from "../shared/store/store-worker.service";

export default class PersonAreaWebrtc {
    peerConnection = new RTCPeerConnection(({ iceServers: ICE_SERVER_CONFIG }));
    dataChannel;
    offer$ = new Subject();
    answer$ = new Subject();
    offer;
    localStream;
    remoteTracks = {};


    createRTCOffer() {
        this.dataChannel = this.peerConnection.createDataChannel('test');
        from(mediaDevices.getUserMedia({
            video: true,
            audio: true
        }))
            .pipe(take(1))
            .subscribe({
                next: (stream) => {
                    let offerCandidate = '';

                    this.peerConnection.onaddstream = e => {
                        this.addRemoteStream(e.stream);
                    };
                    this.addStream(stream);
                    this.dataChannel.onopen = () => {
                        console.log('oppeeeennn');
                        StoreWorkerService.updateCallState('mediaStreams$', this.remoteTracks);
                    };
                    this.peerConnection.onicecandidate = event => {
                        console.log('onicecandidate', event.candidate?.candidate)
                        let data;
                        if (!event.candidate && offerCandidate) {
                            data = {
                                candidate: offerCandidate,
                                offer: this.peerConnection.localDescription
                            }
                        }
                        offerCandidate = event.candidate;
                        if (data) this.offer$.next(data);
                    };
                    this.subscribeToCreateOffer();
                },
                error: () => console.log(error)
            });
        return this.offer$;
    }

    subscribeToCreateOffer() {
        from(this.peerConnection.createOffer())
            .pipe(take(1))
            .subscribe(offer => this.peerConnection.setLocalDescription(new RTCSessionDescription(offer)));
    }


    createRTCAnswer({ offer, candidate }) {
        from(mediaDevices.getUserMedia({
            video: true,
            audio: true
        }))
            .pipe(take(1))
            .subscribe({
                next: (stream) => {
                    let answerCandidate = '';

                    this.peerConnection.onaddstream = e => {
                        this.addRemoteStream(e.stream);
                    };

                    this.addStream(stream);

                    this.peerConnection.onicecandidate = event => {
                        console.log('two onicecandidate', event.candidate);
                        let data;
                        if (!event.candidate && answerCandidate) {
                            data = {
                                candidate: answerCandidate,
                                answer: this.peerConnection.localDescription
                            }
                        }
                        answerCandidate = event.candidate;
                        if (data) this.answer$.next(data);
                    };

                    this.subscribeToSaveOffer(offer, candidate);
                    this.peerConnection.ondatachannel = event => {
                        this.dataChannel = event.channel;
                        this.dataChannel.onopen = () => {
                            StoreWorkerService.updateCallState('mediaStreams$', this.remoteTracks);
                        };
                    };
                    this.subscribeToCreateAnswer();
                },
                error: err => console.log(err)
            })
        return this.answer$;
    }

    subscribeToCreateAnswer() {
        from(this.peerConnection.createAnswer())
            .pipe(take(1))
            .subscribe(answer => this.peerConnection.setLocalDescription(new RTCSessionDescription(answer)));
    }

    subscribeToSaveOffer(offer, candidate) {
        from(this.peerConnection.setRemoteDescription(offer))
            .pipe(take(1))
            .subscribe(() => this.peerConnection.addIceCandidate(new RTCIceCandidate(candidate)));
    }

    saveAnswer({ answer, candidate }) {
        const sessionDescription = new RTCSessionDescription(answer);
        from(this.peerConnection.setRemoteDescription(sessionDescription))
            .pipe(take(1))
            .subscribe(() => this.peerConnection.addIceCandidate(new RTCIceCandidate(candidate)));
    }

    cancelConnection() {
        if (this.localStream) {
            this.localStream.getTracks().forEach(track => track.stop());
        }
        this.peerConnection.close();
        this.peerConnection = null;
    }

    addStream(stream) {
        this.localStream = stream;
        this.peerConnection.addStream(stream);
    }

    addRemoteStream(stream) {
        this.remoteTracks.video = new MediaStream(stream);
    }
}
