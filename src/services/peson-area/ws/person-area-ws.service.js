import WsService from "../../shared/ws/ws-service";
import { Subject } from "rxjs";

export default class PersonAreaWsService {
    static sendData(data) {
        WsService.send(data);
    }

    static listenMessages() {
        const message$ = new Subject();
        WsService.WS.onmessage = (msg) => message$.next(msg);
        return message$.asObservable();
    }
}
