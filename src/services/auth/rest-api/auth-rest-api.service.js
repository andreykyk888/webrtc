import axios from "axios";
import { RestApiConfig } from "../../../classes/api-config";
import { from } from "rxjs";

export default class AuthRestApiService {
    static signUp(userInfo) {
         return from(axios.post(RestApiConfig.signUpPath, userInfo));
    }

    static signIn(userInfo) {
        return from(axios.post(RestApiConfig.signInPath, userInfo));
    }
}