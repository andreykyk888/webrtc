import AuthRestApiService from "./rest-api/auth-rest-api.service";
import { map, tap } from "rxjs";
import AsyncStorageService from "../shared/async-storage.service";
import { USER_STORAGE_KEY } from '../../consts/storage-keys.consts'
import StoreWorkerService from "../shared/store/store-worker.service";
import WsService from "../shared/ws/ws-service";

export default class AuthService {
    static signUp(userInfo) {
        return AuthRestApiService.signUp(userInfo)
            .pipe(
                map((user) => user.data),
                tap((user) => AsyncStorageService.saveItem(user.result, USER_STORAGE_KEY)),
                tap((user) => StoreWorkerService.updateUserState(user.result))
            )
    };

    static signIn(userInfo) {
        return AuthRestApiService.signIn(userInfo)
            .pipe(
                map((user) => user.data),
                tap((user) => AsyncStorageService.saveItem(user.result, USER_STORAGE_KEY)),
                tap((user) => StoreWorkerService.updateUserState(user.result))
            )
    };

    //TODO: this should be provider
    static logout() {
        return AsyncStorageService.removeItem(USER_STORAGE_KEY)
            .pipe(
                tap(() => StoreWorkerService.updateUserState(undefined)),
                tap(() => WsService.disconnect())
            );
    }
}
