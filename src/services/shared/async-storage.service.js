import AsyncStorage from "@react-native-async-storage/async-storage";
import { from, map } from "rxjs";

export default class AsyncStorageService {
    static saveItem(data, key) {
        const storageData = JSON.stringify(data);
        AsyncStorage.setItem(key, storageData);
    };

    static getItem(key) {
        return from(AsyncStorage.getItem(key)).pipe(
            map(v => {
               return v? JSON.parse(v) : v;
            })
        );
    }

    static removeItem(key) {
        return from(AsyncStorage.removeItem(key));
    }


}