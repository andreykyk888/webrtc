import StoreService from "./store.service";

export default class StoreWorkerService {
    static updateUserState(value) {
        StoreService.userState$.next(value);
    };

    static get userStateValue() {
        return StoreService.userState$.getValue();
    };

    static updateSocketConnectionState(value) {
        StoreService.isSocketConnectedState$.next(value);
    }

    static get socketConnectionState$() {
        return StoreService.isSocketConnectedState$.asObservable();
    }

    static get socketConnectionStateValue() {
        return StoreService.isSocketConnectedState$.getValue();
    }

    static updateCallState(key, value) {
        StoreService.callStates[key].next(value);
    }

    static getCallState$(key) {
        return StoreService.callStates[key].asObservable();
    }

    static getCallState(key) {
        return StoreService.callStates[key].getValue();
    }
}
