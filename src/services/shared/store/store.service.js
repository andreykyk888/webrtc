import { BehaviorSubject } from "rxjs";
import { ISNT_CONNECTED_NO_RECONNECT } from "../../../consts/ws-states";

export default class StoreService {
    static userState$ = new BehaviorSubject(null);
    static isSocketConnectedState$ = new BehaviorSubject(ISNT_CONNECTED_NO_RECONNECT);
    static callStates = {
        outgoingCall$: new BehaviorSubject(undefined),
        incomingCall$: new BehaviorSubject(undefined),
        mediaStreams$: new BehaviorSubject(undefined),
    };
}
