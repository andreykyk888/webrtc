import WsApiService from "./ws-api/ws-api.service";
import StoreWorkerService from "../store/store-worker.service";
import { filter, take } from "rxjs";
import {
    CONNECTED_NO_RECONNECT,
    ISNT_CONNECTED_NO_RECONNECT,
    ISNT_CONNECTED_RECONNECT
} from "../../../consts/ws-states";

export default class WsService {
    static WS;

    static connect() {
        WsService.WS = WsApiService.connect();
        WsService.WS.onopen = () => {
            console.log('onopen');
            StoreWorkerService.updateSocketConnectionState(CONNECTED_NO_RECONNECT);
        };
        WsService.WS.onclose = () => {
            console.log('connect');
            StoreWorkerService.updateSocketConnectionState(ISNT_CONNECTED_RECONNECT);
        };
    }

    static send(data) {
        if(StoreWorkerService.socketConnectionStateValue.isConnected) {
            WsApiService.send(data);
        } else {
            WsService.connect();
            StoreWorkerService.socketConnectionState$
                .pipe(
                    filter(v => !!v.isConnected),
                    take(1)
                )
                .subscribe(() => WsApiService.send(data))
        }
    }

    static disconnect() {
        WsService.WS.onclose = () => {
            console.log('disconnect');
            StoreWorkerService.updateSocketConnectionState(ISNT_CONNECTED_NO_RECONNECT);
        };
        WsApiService.disconnect();
    }
}
