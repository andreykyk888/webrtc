import { WsApiConfig } from "../../../../classes/api-config";

export default class WsApiService {
    static WS;
    static connect() {
        WsApiService.WS = new WebSocket(WsApiConfig.home);
        return WsApiService.WS;
    };

    static send(data) {
        WsApiService.WS.send(data);
    }

    static disconnect() {
        WsApiService.WS.close();
    }
}
