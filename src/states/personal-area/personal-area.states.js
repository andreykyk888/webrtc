const sendCallPopupStates = { visible: true, hidden: false };
const callPopupStates = {
    visibleSender: 'visible-offer',
    visibleReceiver: 'visible-answer',
    accepted: 'accepted',
    hidden: 'hidden'
}

export { sendCallPopupStates, callPopupStates };
