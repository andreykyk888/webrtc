export default class SendCallFormStateSetters {
    static setSendCallFormState(key, state, setState) {
        const newState = { [key]: state };
        setState(newState);
    }
}
