export default class AuthStateSetters {
    static setAuthFormState(state, oldState, setState) {
        const newState = {
            ...oldState,
            ...state
        }
        setState(newState);
    }
}