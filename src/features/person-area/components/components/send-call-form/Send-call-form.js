import React, { useState } from 'react';
import { Button, Text, TextInput, View } from "react-native";
import styles from "../../../../../../assets/styles/_common.styles";
import SendCallFormStateSetters from "../../../../../states/send-call-form/send-call-form.state-setters";
import { sendCallFormStyles } from "./styles/Send-call-form.styles";

const SendCallForm = ({sendCallFormConfig}) => {
    const [formState, setFormState] = useState(undefined);

    const onChangeFormState = (key, state) => {
        SendCallFormStateSetters.setSendCallFormState(key, state, setFormState);
    }

    return (
        <View style={sendCallFormStyles.container}>
            <Text style={sendCallFormStyles.title}>Call</Text>
            <View style={sendCallFormStyles.inputBlock}>
                <TextInput style={[sendCallFormStyles.input, styles.input]}
                           onChangeText={(value) =>
                               onChangeFormState('username', value)}
                           placeholder={sendCallFormConfig.fields.username}
                />
            </View>
            <View style={sendCallFormStyles.actionBlock}>
                <Button onPress={sendCallFormConfig.actions.cancel.action}
                        title={sendCallFormConfig.actions.cancel.title}
                />
                <Button onPress={() => sendCallFormConfig.actions.submit.action(formState)}
                        title={sendCallFormConfig.actions.submit.title}
                />
            </View>
        </View>
    );
};

export default SendCallForm;
