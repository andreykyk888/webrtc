import { StyleSheet } from "react-native";

const sendCallFormStyles = StyleSheet.create({
    container: {
        width: 250
    },
    inputBlock: {
        marginTop: 40,
    },
    title: {
        fontSize: 24,
        alignSelf: "center"
    },
    input: {
        marginBottom: 24
    },
    actionBlock: {
        flexDirection: "row",
        justifyContent: "space-between"
    }
});

export { sendCallFormStyles }