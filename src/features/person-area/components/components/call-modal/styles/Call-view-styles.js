import { StyleSheet } from "react-native";

const callViewStyles = StyleSheet.create({
    container: {
        width: 250,
        height: 400,
        backgroundColor: 'rgba(33,174,255,0.68)',
        justifyContent: "center",
        position: "relative"
    },
    text: {
        textAlign: "center",
        alignSelf: "center",
        fontWeight: "bold",
        fontSize: 18,
        color: "#fff"
    },
    actionBlock: {
        width: 200,
        position: 'absolute',
        bottom: 10,
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center"
    },
    video: {
        width: '100%',
        height: '100%'
    }
});

export { callViewStyles }
