import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Button, View } from "react-native";
import { callPopupStates } from "../../../../../states/personal-area/personal-area.states";
import { Text } from "react-native";
import { callViewStyles } from "./styles/Call-view-styles";
import { RTCView } from "react-native-webrtc";
import { skip, take } from "rxjs";

const CallView = ({ user, callPopupState, cancelAction, receiveAction, streamState }) => {
    const [textState, setTextState] = useState('');
    const [mediaStreamsState, setMediaSteamsState] = useState(undefined);
    let isClickedReceive = false;

    useEffect(() => initStates(), []);

    const initStates = useCallback(() => {
        selectContentText();
        subscribeToStreamState();
    }, []);

    const subscribeToStreamState = useCallback(() => {
        streamState
            .pipe(
                skip(1),
                take(1)
            )
            .subscribe(stream => {
                setMediaSteamsState(stream);
            });
    }, [])

    const selectContentText = useCallback(() => {
        let text;
        if (mediaStreamsState) text = '';
        switch (callPopupState) {
            case callPopupStates.visibleSender:
                text = `You are calling to ${user.username}...`;
                break;
            case callPopupStates.visibleReceiver:
                text = `Call from ${user.username}...`;
                break;
        }
        setTextState(text);
    }, [mediaStreamsState]);

    const executeReceiveActions = () => {
        if(isClickedReceive) return;
        console.log('executeReceiveActions')
        receiveAction();
        isClickedReceive = true;
    }

    const selectActions = useMemo(() => {
        if (callPopupState === callPopupStates.visibleSender || mediaStreamsState) {
            return <Button onPress={cancelAction} title={'Cancel'}/>
        } else {
            return <>
                <Button onPress={cancelAction} title={'Cancel'}/>
                <Button disabled={isClickedReceive} onPress={executeReceiveActions} title={'Receive'}/>
            </>
        }
    }, [mediaStreamsState]);

    const selectContent = useMemo(() => {
        if (!mediaStreamsState) {
            return <>
                <Text style={callViewStyles.text}>{textState}</Text>
                <View style={callViewStyles.actionBlock}>
                    {selectActions}
                </View>
            </>
        } else {
            return <>
                <RTCView
                    mirror={true}
                    style={callViewStyles.video}
                    objectFit={'contain'}
                    streamURL={mediaStreamsState.video.toURL()}
                    zOrder={0}
                />
                <View style={callViewStyles.actionBlock}>
                    {selectActions}
                </View>
            </>
        }
    }, [mediaStreamsState, textState]);

    return (
        <View style={callViewStyles.container}>
            {selectContent}
        </View>
    );
};

export default CallView;
