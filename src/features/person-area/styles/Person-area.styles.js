import { StyleSheet } from "react-native";

const personAreaStyles = StyleSheet.create({
    container: {
        paddingHorizontal: 30,
        paddingTop: '20%'
    },
    greetingText: {
        fontSize: 18,
        fontWeight: "bold"
    },

    actionBlock: {
        width: '100%',
        alignSelf: "center",
        justifyContent: "space-around",
        flexDirection: "row",
        marginTop: '70%'
    }
});


export { personAreaStyles }