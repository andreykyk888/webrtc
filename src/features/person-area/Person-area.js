import React, { useCallback, useMemo, useState } from 'react';
import { Button, Text, View } from "react-native";
import StoreWorkerService from "../../services/shared/store/store-worker.service";
import { personAreaStyles } from "./styles/Person-area.styles";
import { callPopupStates, sendCallPopupStates } from "../../states/personal-area/personal-area.states";
import DefaultModal from "../shared/components/wrappers/default-modal/default-modal";
import { SEND_CALL_BTN_TITLE, SEND_CALL_FIELDS, CANCEL_BTN_TITLE } from "../../consts/send-call-form.consts";
import SendCallForm from "./components/components/send-call-form/Send-call-form";
import AuthService from "../../services/auth/auth.service";
import Router from "../../classes/router";
import WsService from "../../services/shared/ws/ws-service";
import PersonAreaService from "../../services/peson-area/person-area.service";
import { filter, Subject, tap, skip, takeUntil } from "rxjs";
import { useFocusEffect } from "@react-navigation/native";
import CallView from "./components/components/call-modal/Call-view";

const PersonArea = ({ navigation }) => {
    const [popupState, setPopupState] = useState(sendCallPopupStates.hidden);
    const [callPopupState, setCallPopupState] = useState(callPopupStates.hidden);
    const [callUserState, setCallUserState] = useState(undefined);
    const [personData, setPersonData] = useState({});
    const mediaStreamsState = StoreWorkerService.getCallState$('mediaStreams$');
    const unsubscribe$ = new Subject();
    const unsubscribeFromMessage$ = new Subject();

    const initVariables = () => {
        const { firstName, lastName } = StoreWorkerService.userStateValue;
        setPersonData({ firstName, lastName });
    };

    const unsubscribe = () => {
        unsubscribe$.next();
        unsubscribeFromMessage$.next();
    };

    const subscribeToMessages = () => {
        PersonAreaService.listenMessages()
            .pipe(takeUntil(unsubscribeFromMessage$))
            .subscribe(message => {
                PersonAreaService.selectAction(message.data);
            });
    };

    const subscribeToConnectionState = () => {
        StoreWorkerService.socketConnectionState$
            .pipe(
                filter(v => !!v.isConnected),
                takeUntil(unsubscribe$),
                tap(() => unsubscribeFromMessage$.next())
            )
            .subscribe(() => {
                subscribeToMessages();
                PersonAreaService.sendUserData();
            });
        StoreWorkerService.socketConnectionState$
            .pipe(
                filter(v => !!v.isReconnect),
                takeUntil(unsubscribe$)
            )
            .subscribe(() => {
                connectToWs();
            });
    };

    const connectToWs = () => {
        WsService.connect();
    }

    const initWs = () => {
        subscribeToConnectionState();
        connectToWs();
    };

    const initSubscribers = () => {
        subscribeToCallStates();
    }

    useFocusEffect(useCallback(() => {
        unsubscribe();
        initVariables();
        initWs();
        initSubscribers();
    }, []));


    const hidePopup = () => {
        setPopupState(sendCallPopupStates.hidden);
    };

    const openPopup = () => {
        setPopupState(sendCallPopupStates.visible);
    };

    const cancelCallFromSender = () => {
        PersonAreaService.cancelOffer();
    }

    const cancelCallFromReceiver = () => {
        PersonAreaService.cancelAnswer();
    }

    const sendAnswer = () => {
        PersonAreaService.sendAnswerData();
    }

    const subscribeToCallStates = () => {
        StoreWorkerService.getCallState$('outgoingCall$')
            .pipe(
                skip(1),
                takeUntil(unsubscribe$)
            )
            .subscribe((offerData) => {
                if (!offerData) {
                    setCallPopupState(callPopupStates.hidden);
                    return;
                }
                const { receiverUsername } = offerData;
                const receiver = { username: receiverUsername };
                setCallUserState(receiver);
                setCallPopupState(callPopupStates.visibleSender);
            });
        StoreWorkerService.getCallState$('incomingCall$')
            .pipe(
                skip(1),
                takeUntil(unsubscribe$)
            )
            .subscribe((offerData) => {
                if (!offerData) {
                    setCallPopupState(callPopupStates.hidden);
                    return;
                }
                setCallUserState(offerData.sender);
                setCallPopupState(callPopupStates.visibleReceiver);
            });
    };

    const sendCall = (data) => {
        PersonAreaService.sendOfferData(data.username);
        hidePopup();
    };

    const logout = () => {
        AuthService.logout()
            .pipe(takeUntil(unsubscribe$))
            .subscribe(() => {
                navigation.navigate(Router.greetingScreen);
                unsubscribe();
            });
    };

    const sendCallFormConfig = {
        fields: SEND_CALL_FIELDS,
        actions: {
            submit: {
                title: SEND_CALL_BTN_TITLE,
                action: sendCall,
            },
            cancel: {
                title: CANCEL_BTN_TITLE,
                action: hidePopup
            }
        },
    };

    const showSendCallPopup = useMemo(() => {
        if (popupState) {
            return (
                <DefaultModal>
                    <SendCallForm sendCallFormConfig={sendCallFormConfig}/>
                </DefaultModal>
            )
        }
    }, [popupState]);

    const showCallPopup = useMemo(() => {
        switch (callPopupState) {
            case callPopupStates.visibleSender:
                return (
                    <DefaultModal>
                        <CallView
                            callPopupState={callPopupState}
                            user={callUserState}
                            streamState={mediaStreamsState}
                            cancelAction={cancelCallFromSender}
                        />
                    </DefaultModal>
                )
            case callPopupStates.visibleReceiver:
                return (
                    <DefaultModal>
                        <CallView callPopupState={callPopupState}
                                  user={callUserState}
                                  streamState={mediaStreamsState}
                                  cancelAction={cancelCallFromReceiver}
                                  receiveAction={sendAnswer}
                        />
                    </DefaultModal>
                )
        }
    }, [callPopupState]);

    return (
        <View style={personAreaStyles.container}>
            {showSendCallPopup}
            {showCallPopup}
            <Text
                style={personAreaStyles.greetingText}>{`Hello ${personData.firstName} ${personData.lastName}!`}</Text>
            <View style={personAreaStyles.actionBlock}>
                <Button title={'Call'} onPress={openPopup}/>
                <Button title={'Logout'} onPress={logout}/>
            </View>
        </View>
    );
};

export default PersonArea;
