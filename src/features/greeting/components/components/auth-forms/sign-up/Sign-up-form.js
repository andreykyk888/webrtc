import React, { useState } from 'react';
import { Button, Text, TextInput, View } from "react-native";
import styles from "../../../../../../../assets/styles/_common.styles";
import AuthStateSetters from "../../../../../../states/auth-forms/auth.state-setters";
import { authFormsStyles } from "../shared/styles/Auth-forms.styles";

const SignUpForm = ({ signUpConfig }) => {
    const [formState, setFormState] = useState({});

    const onChangeFormState = (key, state) => {
        const stateObj = { [key]: state };
        AuthStateSetters.setAuthFormState(stateObj, formState, setFormState);
    };

    return (
            <View style={authFormsStyles.container}>
                <Text style={authFormsStyles.title}>Sign up</Text>
                <View style={authFormsStyles.inputBlock}>
                    <TextInput style={[authFormsStyles.input, styles.input]}
                               onChangeText={(value) => onChangeFormState('firstName', value)}
                               placeholder={signUpConfig.fields.firstName}
                    />
                    <TextInput style={[authFormsStyles.input, styles.input]}
                               onChangeText={(value) => onChangeFormState('lastName', value)}
                               placeholder={signUpConfig.fields.lastName}
                    />
                    <TextInput style={[authFormsStyles.input, styles.input]}
                               onChangeText={(value) => onChangeFormState('username', value)}
                               placeholder={signUpConfig.fields.username}
                    />
                </View>
                <View style={authFormsStyles.actionBlock}>
                    <Button onPress={signUpConfig.actions.cancel.action}
                            title={signUpConfig.actions.cancel.title}
                    />
                    <Button onPress={() => signUpConfig.actions.submit.action(formState)}
                            title={signUpConfig.actions.submit.title}
                    />
                </View>
            </View>
    );
};

export default SignUpForm;