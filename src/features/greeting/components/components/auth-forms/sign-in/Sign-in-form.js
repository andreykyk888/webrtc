import React, { useState } from 'react';
import { Button, Text, TextInput, View } from "react-native";
import { authFormsStyles } from "../shared/styles/Auth-forms.styles";
import styles from "../../../../../../../assets/styles/_common.styles";
import AuthStateSetters from "../../../../../../states/auth-forms/auth.state-setters";

const SignInForm = ({ signInConfig }) => {
    const [formState, setFormState] = useState({});

    const onChangeFormState = (key, state) => {
        const stateObj = { [key]: state };
        AuthStateSetters.setAuthFormState(stateObj, formState, setFormState);
    }

    return (
        <View style={authFormsStyles.container}>
            <Text style={authFormsStyles.title}>Sign in</Text>
            <View style={authFormsStyles.inputBlock}>
                <TextInput style={[authFormsStyles.input, styles.input]}
                           onChangeText={(value) =>
                               onChangeFormState('username', value)}
                           placeholder={signInConfig.fields.username}
                />
            </View>
            <View style={authFormsStyles.actionBlock}>
                <Button onPress={signInConfig.actions.cancel.action}
                        title={signInConfig.actions.cancel.title}
                />
                <Button onPress={() => signInConfig.actions.submit.action(formState)}
                        title={signInConfig.actions.submit.title}
                />
            </View>
        </View>
    );
};

export default SignInForm;