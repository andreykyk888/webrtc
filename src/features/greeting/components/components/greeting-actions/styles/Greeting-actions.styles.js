import { StyleSheet } from "react-native";

const greetingActionsStyles = StyleSheet.create({
    actionsContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
    }
});

export default greetingActionsStyles;