import React from 'react';
import { Button, View } from "react-native";
import { appBtn } from "../../../../shared/styles/Shared.styles";
import greetingActionsStyles from "./styles/Greeting-actions.styles";

const GreetingActions = ({ actionsConfig }) => {
    const { signInConfig, signUpConfig } = actionsConfig;
    return (
        <View style={greetingActionsStyles.actionsContainer}>
            <Button onPress={signInConfig.action}
                    title={signInConfig.title}
            />
            <Button onPress={signUpConfig.action}
                    title={signUpConfig.title}
            />
        </View>
    );
};

export default GreetingActions;