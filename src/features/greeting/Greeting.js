import React, { useMemo, useState } from 'react';
import { ImageBackground, View } from "react-native";
import greetingStyles from "./styles/Greeting.styles";
import GreetingActions from "./components/components/greeting-actions/Greeting-actions";
import { popupStates } from "../../states/greeting/greeting.states";
import {
    CANCEL_BTN_TITLE,
    SIGN_IN_BTN_TITLE,
    SIGN_IN_FIELDS,
    SIGN_UP_BTN_TITLE,
    SIGN_UP_FIELDS
} from "../../consts/auth-forms.consts";
import SignUpForm from "./components/components/auth-forms/sign-up/Sign-up-form";
import AuthService from "../../services/auth/auth.service";
import SignInForm from "./components/components/auth-forms/sign-in/Sign-in-form";
import DefaultModal from "../shared/components/wrappers/default-modal/default-modal";
import { Subject, takeUntil } from "rxjs";

const Greeting = ({ navigation }) => {
    const [popupState, setPopupState] = useState(popupStates.hidden);
    const unsubscribe$ = new Subject();

    const openSignIn = () => {
        setPopupState(popupStates.signIn);
    };

    const openSignUp = () => {
        setPopupState(popupStates.signUp);
    };

    const hidePopup = () => {
        setPopupState(popupStates.hidden);
    };

    const onSignUpClick = (userInfo) => {
        AuthService.signUp(userInfo)
            .pipe(takeUntil(unsubscribe$))
            .subscribe({
                next: () => {
                    hidePopup();
                    navigation.navigate('personal-area');
                    unsubscribe$.next();
                },
                error: error => {
                    console.log(error);
                    unsubscribe$.next();
                },
                complete: () => console.log('completed')
            });
    };

    const onSignInClick = (userInfo) => {
        AuthService.signIn(userInfo)
            .pipe(takeUntil(unsubscribe$))
            .subscribe({
                next: () => {
                    hidePopup();
                    navigation.navigate('personal-area');
                    unsubscribe$.next();
                },
                error: error => {
                    console.log(error);
                    unsubscribe$.next();
                },
                complete: () => console.log('completed')
            });
    };

    const signInActionConfig = {
        title: SIGN_IN_BTN_TITLE,
        action: openSignIn
    };

    const signUpActionConfig = {
        title: SIGN_UP_BTN_TITLE,
        action: openSignUp
    };

    const actionsConfig = {
        signInConfig: signInActionConfig,
        signUpConfig: signUpActionConfig
    };

    const signUpFormConfig = {
        fields: SIGN_UP_FIELDS,
        actions: {
            submit: {
                title: SIGN_IN_BTN_TITLE,
                action: onSignUpClick,
            },
            cancel: {
                title: CANCEL_BTN_TITLE,
                action: hidePopup
            }
        },
    };

    const signInFormConfig = {
        fields: SIGN_IN_FIELDS,
        actions: {
            submit: {
                title: SIGN_IN_BTN_TITLE,
                action: onSignInClick,
            },
            cancel: {
                title: CANCEL_BTN_TITLE,
                action: hidePopup
            }
        },
    };

    const selectForm = (popupState) => {
        switch (popupState) {
            case popupStates.signUp:
                return <SignUpForm signUpConfig={signUpFormConfig}/>;
            case popupStates.signIn:
                return <SignInForm signInConfig={signInFormConfig}/>
        }
    };

    const showModal = useMemo(() => {
        if (!popupState) return;
        return (
            <DefaultModal>
                {selectForm(popupState)}
            </DefaultModal>
        )
    }, [popupState]);

    return (
        <ImageBackground resizeMode="contain"
                         style={greetingStyles.imageBackground}
                         source={require("../../../assets/images/greeting/greeting_background.jpeg")}>
            <View style={greetingStyles.actionsContainer}>
                {showModal}
                <GreetingActions actionsConfig={actionsConfig}/>
            </View>
        </ImageBackground>
    );
};

export default Greeting;