import { StyleSheet } from "react-native";

const greetingStyles = StyleSheet.create({
    imageBackground: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        position: 'relative',
        backgroundColor: '#fff',
    },
    actionsContainer: {
        width: '70%',
        position: 'absolute',
        bottom: '20%',
    },
})

export default greetingStyles