import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import Greeting from "../../greeting/Greeting";
import PersonArea from "../../person-area/Person-area";
import Router from "../../../classes/router";
import StoreWorkerService from "../../../services/shared/store/store-worker.service";

const Stack = createStackNavigator();

export default function navigation() {
    return (
        <NavigationContainer>
            <Stack.Navigator
                initialRouteName={StoreWorkerService.userStateValue ? Router.personalAreaScreen : Router.greetingScreen}
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen
                    name={'personal-area'}
                    component={PersonArea}
                />
                <Stack.Screen
                    name={'greeting'}
                    component={Greeting}
                />
            </Stack.Navigator>
        </NavigationContainer>
    )
}