import { environment } from "../../environment/environment";

class RestApiConfig {
    static home = `${environment.host}/api`
    static signUpPath = `${RestApiConfig.home}/users/register`;
    static signInPath = `${RestApiConfig.home}/users/login`;
}

class WsApiConfig {
    static home = `${environment.wsHost}/api`
}

export {
    RestApiConfig,
    WsApiConfig
}