import { StatusBar } from 'expo-status-bar';
import { View } from 'react-native';
import appStyles from "./App.styles";
import ComponentsStack from "./src/features/shared/navigation/navigation";
import { Component } from "react";
import AsyncStorageService from "./src/services/shared/async-storage.service";
import { USER_STORAGE_KEY } from "./src/consts/storage-keys.consts";
import StoreWorkerService from "./src/services/shared/store/store-worker.service";

export default class App extends Component {
    constructor() {
        super();
        this.state = {};
    }

    componentDidMount() {
        this.updateUserState();
    }

    updateUserState() {
        this.setState({ isLoading: true });
        AsyncStorageService.getItem(USER_STORAGE_KEY).subscribe(user => {
            StoreWorkerService.updateUserState(user);
            this.setState({ isLoading: false });
        })
    }

    render() {
        if (!this.state.isLoading) {
            return (
                <View style={appStyles.container}>
                    <StatusBar style="auto"/>
                    <ComponentsStack/>
                </View>
            )
        }
        return <></>
    }
}
