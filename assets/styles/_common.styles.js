import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    input: {
        height: 30,
        width: '100%',
        fontSize: 18,
        borderWidth: 1,
        padding: 4
    }

});

export default styles