import {StyleSheet} from "react-native";

const appStyles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: "center"
    },

});

export default appStyles